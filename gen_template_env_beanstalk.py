"""
Generate cloudformation template with troposphere
"""
#!/usr/bin/python
#pylint: disable=invalid-name
#pylint: disable=redefined-builtin
#pylint: disable=superfluous-parens
import os
import argparse
from troposphere import Template, ImportValue
from troposphere.elasticbeanstalk import Environment, OptionSettings

# Arguments

PARSER = argparse.ArgumentParser(description='Generate cloudformation template.')
PARSER.add_argument('-p', '--project',
                    help='GROUPALEXI1',
                    required=True)
PARSER.add_argument('-e', '--env',
                    help='dev',
                    required=True)
PARSER.add_argument('-a', '--app',
                    help='GROUPALEXI1APP',
                    required=True)

ARGS = PARSER.parse_args()

# Variables

PROJECT = ARGS.project.upper()
APP = ARGS.app.upper()
ENV = ARGS.env.upper()

# ADJUST VARIABLES
PROJECT_UND = PROJECT.replace('-', '_')
PROJECT_UND_LOWER = PROJECT_UND.lower()
APP_UND = APP.replace('-', '_')
APP_UND_LOWER = APP_UND.lower()
ENV_LOWER = ENV.lower()

print ('parameters :')
print ('project  : %s' % PROJECT)
print ('app      : %s' % APP)
print ('env      : %s' % ENV)


# TEMPLATE DECLARATION
t = Template()
t.set_version()
t.set_description("Stack %s %s" % (PROJECT, APP))

## BEANSTALK APP
eb_env = t.add_resource(Environment(
    "%s%sBeanstalkEnv%s" % (PROJECT.title(), APP.title(), ENV.title()),
    ApplicationName="%s" % (APP),
    CNAMEPrefix="%s-%s" % (APP_UND_LOWER, ENV_LOWER),
    Description="%s Beanstalk application for %s project" % (APP.title(),
                                                             PROJECT_UND_LOWER.title()),
    EnvironmentName="%s-%s" % (APP, ENV),
    SolutionStackName="64bit Amazon Linux 2018.03 v2.9.4 running PHP 7.3",
    OptionSettings=[
        OptionSettings(
            Namespace="aws:autoscaling:launchconfiguration",
            OptionName="IamInstanceProfile",
            Value=ImportValue("%s%sInstanceProfile" % (PROJECT.title(), APP.title()))
        ),
        OptionSettings(
            Namespace="aws:ec2:instances",
            OptionName="InstanceTypes",
            Value="t3.micro"
        ),
        OptionSettings(
            Namespace="aws:elasticbeanstalk:environment",
            OptionName="EnvironmentType",
            Value="LoadBalanced"
        ),
        OptionSettings(
            Namespace="aws:autoscaling:asg",
            OptionName="MinSize",
            Value="1"
        ),
        OptionSettings(
            Namespace="aws:autoscaling:asg",
            OptionName="MaxSize",
            Value="1"
        )
    ]
))

# OUTPUT
output_dir = "templates"
try:
    os.makedirs(output_dir)
except OSError:
    if not os.path.isdir(output_dir):
        raise

output_file = "{dir}/beanstalk_env.json".format(dir=output_dir)
file = open(output_file, "w")
file.write(t.to_json())
print("cloudformation template generated :", output_file)
