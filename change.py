"""
Generate backend
"""
#!/usr/bin/python
#pylint: disable=invalid-name
#pylint: disable=redefined-builtin
#pylint: disable=line-too-long
import os
from troposphere import Template, Ref, Tags, ec2, cloudwatch

# TEMPLATE DECLARATION
t = Template()
t.set_version()
t.set_description("Template Stack Troposphere")

grafana_sg = t.add_resource(ec2.SecurityGroup(
    'GrafanaInstanceSecurityGroup',
    GroupDescription='Allows SSH access from anywhere',
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol='tcp',
            FromPort=22,
            ToPort=22,
            CidrIp='0.0.0.0/0'
        ),
        ec2.SecurityGroupRule(
            IpProtocol='tcp',
            FromPort=3000,
            ToPort=3000,
            CidrIp='0.0.0.0/0'
        )
    ],
))

grafana_instance = t.add_resource(ec2.Instance(
    "BackendGrafanaServer",
    ImageId="ami-01a5429bb4e4187ed",
    InstanceType="t2.micro",
    KeyName="lxs",
    SecurityGroups=[Ref(grafana_sg)],
    Tags=Tags(
        Name="BackendGrafanaServeralexisgroupe1"
    )
))

grafana_alarm_cpu = t.add_resource(cloudwatch.Alarm(
    "GrafanaCpuUtilizationAlarm",
    AlarmDescription="Alarm if CPU is Greater than 10%",
    Namespace="AWS/EC2",
    MetricName="CPUUtilization",
    Dimensions=[
        cloudwatch.MetricDimension(
            Name="Name",
            Value=Ref(grafana_instance)
        ),
    ],
    Statistic="Average",
    Period="60",
    EvaluationPeriods="1",
    Threshold="5",
    ComparisonOperator="GreaterThanThreshold",
    AlarmActions=[
        "arn:aws:sns:eu-west-1:341182299156:deployment-check"
    ],
))

# OUTPUT
output_dir = "templates"
try:
    os.makedirs(output_dir)
except OSError:
    if not os.path.isdir(output_dir):
        raise

output_file = "{dir}/stack_backend.json".format(dir=output_dir)
file = open(output_file, "w")
file.write(t.to_json())
print("cloudformation template generated :", output_file)
