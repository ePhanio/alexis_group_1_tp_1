"""
Generate cloudformation template with troposphere
"""
#!/usr/bin/python
#pylint: disable=invalid-name
#pylint: disable=redefined-builtin
#pylint: disable=no-value-for-parameter
#pylint: disable=superfluous-parens
import os
import argparse
from troposphere import Template, Ref, Output, Export
from troposphere.iam import (
    Role, PolicyType, InstanceProfile
)
from troposphere.elasticbeanstalk import Application
from awacs.aws import Allow, Statement, Action, Principal, PolicyDocument
from awacs.sts import AssumeRole

# Arguments

PARSER = argparse.ArgumentParser(description='Generate cloudformation template.')
PARSER.add_argument('-p', '--project',
                    help='group_alexi_1',
                    required=True)
PARSER.add_argument('-a', '--app',
                    help='group_alexi_1_app',
                    required=True)

ARGS = PARSER.parse_args()

# Variables

PROJECT = ARGS.project.upper()
APP = ARGS.app.upper()

# ADJUST VARIABLES
PROJECT_UNDESCORE = PROJECT.replace('-', '_')
PROJECT_UNDESCORE_LOWER = PROJECT_UNDESCORE.lower()
APP_UNDESCORE = APP.replace('-', '_')
APP_UNDESCORE_LOWER = APP_UNDESCORE.lower()

print ('parameters :')
print ('project  : %s' % (PROJECT))
print ('app      : %s' % (APP))


# TEMPLATE DECLARATION
t = Template()
t.set_version()
t.set_description("Stack %s %s" % (PROJECT, APP))

## ROLE
role = t.add_resource(Role(
    "%s%sRole" % (PROJECT.title(), APP.title()),
    AssumeRolePolicyDocument=PolicyDocument(
        Statement=[
            Statement(
                Effect=Allow,
                Action=[AssumeRole],
                Principal=Principal("Service", "ec2.amazonaws.com")
            )
        ]
    ),
    Path="/"
))

policy = t.add_resource(PolicyType(
    "%s%sPolicy" % (PROJECT.title(), APP.title()),
    PolicyName="WebServerRole",
    PolicyDocument=PolicyDocument(
        Statement=[
            Statement(Effect=Allow, NotAction=Action("iam", "*"),
                      Resource=["*"])
        ]
    ),
    Roles=[Ref(role)]
))

instance_profile = t.add_resource(InstanceProfile(
    "%s%sInstanceProfile" % (PROJECT.title(), APP.title()),
    Path="/",
    Roles=[Ref(role)]
))

## BEANSTALK APP
eb_app = t.add_resource(Application(
    "%s%sBeanstalkApplication" % (PROJECT.title(), APP.title()),
    ApplicationName="%s" % (APP),
    Description="%s Beanstalk application for %s project" % (APP.title(),
                                                             PROJECT_UNDESCORE_LOWER.title())
))

## Output
t.add_output(
    Output(
        "%s%sInstanceProfileExport" % (PROJECT.title(), APP.title()),
        Description="%s%sInstanceProfile" % (PROJECT.title(), APP.title()),
        Value=Ref(instance_profile),
        Export=Export(instance_profile.title)
    )
)

# OUTPUT
output_dir = "templates"
try:
    os.makedirs(output_dir)
except OSError:
    if not os.path.isdir(output_dir):
        raise

output_file = "{dir}/beanstalk_app.json".format(dir=output_dir)
file = open(output_file, "w")
file.write(t.to_json())
print("cloudformation template generated :", output_file)
